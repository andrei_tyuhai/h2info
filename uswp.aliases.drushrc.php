<?php

// Local installation for access from outside the vm
$aliases['local'] = array(
  'uri' => 'http://10.11.12.14',
  'root' => '/vagrant/public',
  'remote-host' => '10.11.12.14',
  'remote-user' => 'vagrant',
  'ssh-options' => "-i ~/.vagrant.d/insecure_private_key -l vagrant",
  'databases' => array (
    'default' => array (
      'default' => array (
        'database' => 'drupal',
        'username' => 'drupal',
        'password' => 'drupal',
        'host' => '10.11.12.14',
        'port' => '',
        'driver' => 'mysql',
        'prefix' => '',
      ),
    ),
  ),
);

// Vagrant alias for access within the vm
$aliases['vagrant'] = array(
  'root' => '/vagrant/public',
  'databases' => array (
    'default' => array (
      'default' => array (
        'database' => 'drupal',
        'username' => 'drupal',
        'password' => 'drupal',
        'host' => 'localhost',
        'port' => '',
        'driver' => 'mysql',
        'prefix' => '',
      ),
    ),
  ),
);

/**
 * Pantheon site aliases
 */
$aliases['dev'] = array(
  'uri' => 'dev-uswp.gotpantheon.com',
  'db-url' => 'mysql://pantheon:6ad4c88373904ce6bf61746e66054aa2@dbserver.dev.42ba2826-cd0a-432f-89a9-4a0f8bece875.drush.in:12743/pantheon',
  'db-allows-remote' => TRUE,
  'remote-host' => 'appserver.dev.42ba2826-cd0a-432f-89a9-4a0f8bece875.drush.in',
  'remote-user' => 'dev.42ba2826-cd0a-432f-89a9-4a0f8bece875',
  'ssh-options' => '-p 2222 -o "AddressFamily inet"',
  'path-aliases' => array(
    '%files' => 'code/sites/default/files',
    '%drush-script' => 'drush',
  ),
);
$aliases['test'] = array(
  'uri' => 'test-uswp.gotpantheon.com',
  'db-url' => 'mysql://pantheon:17d53eef9d794085b2570d743f107ea6@dbserver.test.42ba2826-cd0a-432f-89a9-4a0f8bece875.drush.in:10314/pantheon',
  'db-allows-remote' => TRUE,
  'remote-host' => 'appserver.test.42ba2826-cd0a-432f-89a9-4a0f8bece875.drush.in',
  'remote-user' => 'test.42ba2826-cd0a-432f-89a9-4a0f8bece875',
  'ssh-options' => '-p 2222 -o "AddressFamily inet"',
  'path-aliases' => array(
    '%files' => 'code/sites/default/files',
    '%drush-script' => 'drush',
  ),
);
$aliases['live'] = array(
  'uri' => 'live-uswp.gotpantheon.com',
  'db-url' => 'mysql://pantheon:e7cb517c38ad45dc88713adebab98d4e@dbserver.live.42ba2826-cd0a-432f-89a9-4a0f8bece875.drush.in:11332/pantheon',
  'db-allows-remote' => TRUE,
  'remote-host' => 'appserver.live.42ba2826-cd0a-432f-89a9-4a0f8bece875.drush.in',
  'remote-user' => 'live.42ba2826-cd0a-432f-89a9-4a0f8bece875',
  'ssh-options' => '-p 2222 -o "AddressFamily inet"',
  'path-aliases' => array(
    '%files' => 'code/sites/default/files',
    '%drush-script' => 'drush',
  ),
);
