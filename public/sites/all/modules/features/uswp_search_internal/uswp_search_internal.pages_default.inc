<?php
/**
 * @file
 * uswp_search_internal.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function uswp_search_internal_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'search_result';
  $page->task = 'page';
  $page->admin_title = 'Search Page';
  $page->admin_description = '';
  $page->path = 'site-search';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_search_result_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'search_result';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'sidebar';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'preface' => NULL,
      'main' => NULL,
      'sidebar' => NULL,
      'featured' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '63b2e4e1-2305-4b10-94c8-4315db9a6596';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d3d1c48c-222e-4b8b-abef-21aadb660e38';
    $pane->panel = 'main';
    $pane->type = 'views_panes';
    $pane->subtype = 'solr_listing-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'exposed' => array(
        'type' => 'All',
        'search_api_views_fulltext_1_op' => 'OR',
        'query' => '',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd3d1c48c-222e-4b8b-abef-21aadb660e38';
    $display->content['new-d3d1c48c-222e-4b8b-abef-21aadb660e38'] = $pane;
    $display->panels['main'][0] = 'new-d3d1c48c-222e-4b8b-abef-21aadb660e38';
    $pane = new stdClass();
    $pane->pid = 'new-dee31410-43a8-4590-8517-f8e7360aa498';
    $pane->panel = 'main';
    $pane->type = 'pane';
    $pane->subtype = 'post_search_guidance';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'dee31410-43a8-4590-8517-f8e7360aa498';
    $display->content['new-dee31410-43a8-4590-8517-f8e7360aa498'] = $pane;
    $display->panels['main'][1] = 'new-dee31410-43a8-4590-8517-f8e7360aa498';
    $pane = new stdClass();
    $pane->pid = 'new-08f15d23-d49f-4310-bac2-3274eceb4b51';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-wjvEvl3NbaqdcWRBvGwsGK1AyZgOXytQ';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Theme/Subtheme',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '08f15d23-d49f-4310-bac2-3274eceb4b51';
    $display->content['new-08f15d23-d49f-4310-bac2-3274eceb4b51'] = $pane;
    $display->panels['sidebar'][0] = 'new-08f15d23-d49f-4310-bac2-3274eceb4b51';
    $pane = new stdClass();
    $pane->pid = 'new-29032cdd-af71-4445-8998-a337eb10bb69';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-Xd1mTF7YIZOPK4W519miI6mOECakr1Zo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Resource Type',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '29032cdd-af71-4445-8998-a337eb10bb69';
    $display->content['new-29032cdd-af71-4445-8998-a337eb10bb69'] = $pane;
    $display->panels['sidebar'][1] = 'new-29032cdd-af71-4445-8998-a337eb10bb69';
    $pane = new stdClass();
    $pane->pid = 'new-647ae008-968a-464c-8b7a-c9729f565d93';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-EpcZg0lNSYU6bhgXM1RYoRHxvgryov1K';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'File Type',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '647ae008-968a-464c-8b7a-c9729f565d93';
    $display->content['new-647ae008-968a-464c-8b7a-c9729f565d93'] = $pane;
    $display->panels['sidebar'][2] = 'new-647ae008-968a-464c-8b7a-c9729f565d93';
    $pane = new stdClass();
    $pane->pid = 'new-ef8322aa-c07b-49e1-a18a-c6f313f7ce74';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-bqtmOvF39VjfveVFqRCqgtFaBnNTpKC2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Publication Date',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'ef8322aa-c07b-49e1-a18a-c6f313f7ce74';
    $display->content['new-ef8322aa-c07b-49e1-a18a-c6f313f7ce74'] = $pane;
    $display->panels['sidebar'][3] = 'new-ef8322aa-c07b-49e1-a18a-c6f313f7ce74';
    $pane = new stdClass();
    $pane->pid = 'new-4fc658b3-99fa-4fed-a6ed-1488fc1741f1';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-Ha4WS3U2oaAkPSPOGgLtMNvnVTppOHAg';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Region/Country',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '4fc658b3-99fa-4fed-a6ed-1488fc1741f1';
    $display->content['new-4fc658b3-99fa-4fed-a6ed-1488fc1741f1'] = $pane;
    $display->panels['sidebar'][4] = 'new-4fc658b3-99fa-4fed-a6ed-1488fc1741f1';
    $pane = new stdClass();
    $pane->pid = 'new-18d42389-fcf1-493b-98bd-e4d16452807f';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-C9449KN7s5ADLm0Y0kvysTGw0tSIL1OG';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Resource Scale',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '18d42389-fcf1-493b-98bd-e4d16452807f';
    $display->content['new-18d42389-fcf1-493b-98bd-e4d16452807f'] = $pane;
    $display->panels['sidebar'][5] = 'new-18d42389-fcf1-493b-98bd-e4d16452807f';
    $pane = new stdClass();
    $pane->pid = 'new-4742e989-eec5-4c57-ad0d-8f1f101924a2';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-szZ3efQQC3kcsCupvOhdNx0r2DSFH7r2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Organization',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = '4742e989-eec5-4c57-ad0d-8f1f101924a2';
    $display->content['new-4742e989-eec5-4c57-ad0d-8f1f101924a2'] = $pane;
    $display->panels['sidebar'][6] = 'new-4742e989-eec5-4c57-ad0d-8f1f101924a2';
    $pane = new stdClass();
    $pane->pid = 'new-5fe255c6-8726-42b7-8590-554c326eaacd';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-zrdjnS5Yr5N2NTvWozSVrBsbOigHIWHz';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Nexus Tag',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = '5fe255c6-8726-42b7-8590-554c326eaacd';
    $display->content['new-5fe255c6-8726-42b7-8590-554c326eaacd'] = $pane;
    $display->panels['sidebar'][7] = 'new-5fe255c6-8726-42b7-8590-554c326eaacd';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-d3d1c48c-222e-4b8b-abef-21aadb660e38';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['search_result'] = $page;

  return $pages;

}
