<?php
/**
 * @file
 * uswp_search_internal.pane.inc
 */

/**
 * Implements hook_default_pane_container().
 */
function uswp_search_internal_default_pane_container() {
  $export = array();

  $template = new stdClass();
  $template->disabled = FALSE; /* Edit this to true to make a default template disabled initially */
  $template->api_version = 2;
  $template->name = 'post_search_guidance';
  $template->title = 'Post-Search Guidance';
  $template->plugin = 'text';
  $template->description = 'Further options if searches didn\'t include desired results.';
  $template->configuration = '';
  $export['post_search_guidance'] = $template;

  return $export;
}
