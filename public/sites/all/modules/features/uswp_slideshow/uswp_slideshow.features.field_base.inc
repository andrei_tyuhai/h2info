<?php
/**
 * @file
 * uswp_slideshow.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uswp_slideshow_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'eq_asset'
  $field_bases['eq_asset'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'entityqueue_subqueue',
    ),
    'field_name' => 'eq_asset',
    'foreign keys' => array(
      'asset' => array(
        'columns' => array(
          'target_id' => 'aid',
        ),
        'table' => 'asset',
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 1,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'entityqueue',
      'handler_settings' => array(
        'behaviors' => array(
          'entityqueue' => array(
            'status' => 1,
          ),
        ),
      ),
      'target_type' => 'asset',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
