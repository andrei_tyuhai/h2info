<?php
/**
 * @file
 * uswp_slideshow.features.inc
 */

/**
 * Implements hook_default_asset_type().
 */
function uswp_slideshow_default_asset_type() {
  $items = array();
  $items['slide'] = entity_import('asset_type', '{
    "type" : "slide",
    "name" : "Slide",
    "icon" : "none",
    "description" : "A \\u003Cem\\u003Eslide\\u003C\\/em\\u003E is an individual frame to be used in a featured image carousel.",
    "help" : "",
    "weight" : "0",
    "locked" : "0",
    "uuid" : ""
  }');
  return $items;
}

/**
 * Implements hook_ccl_features_preset().
 */
function uswp_slideshow_ccl_features_preset() {
  return array(
    'Manage slideshow' => array(
      'type' => 'view',
      'title' => 'Manage slideshow',
      'link' => 'admin/structure/entityqueue/list/homepage_carousel/subqueues/homepage_carousel/edit?destination=[current-page:url:relative]',
      'options' => 'a:9:{s:12:"advanced_css";s:0:"";s:14:"advanced_query";s:0:"";s:15:"advanced_target";s:7:"default";s:12:"node_options";s:4:"node";s:9:"node_type";s:9:"uswp_help";s:7:"node_id";s:7:" [nid:]";s:12:"view_options";s:1:"2";s:12:"view_display";s:27:"slideshow|homepage_carousel";s:9:"view_view";s:13:"term_children";}',
    ),
  );
}

/**
 * Implements hook_ctools_plugin_api().
 */
function uswp_slideshow_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_entityqueue_api().
 */
function uswp_slideshow_entityqueue_api($module = NULL, $api = NULL) {
  if ($module == "entityqueue" && $api == "entityqueue_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uswp_slideshow_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function uswp_slideshow_image_default_styles() {
  $styles = array();

  // Exported image style: full_width_slide.
  $styles['full_width_slide'] = array(
    'name' => 'full_width_slide',
    'label' => 'Full-width Slide (1100x450)',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1100,
          'height' => 450,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
