<?php
/**
 * @file
 * uswp_slideshow.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uswp_slideshow_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer flexslider'.
  $permissions['administer flexslider'] = array(
    'name' => 'administer flexslider',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'flexslider',
  );

  // Exported permission: 'create asset with type slide'.
  $permissions['create asset with type slide'] = array(
    'name' => 'create asset with type slide',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'asset',
  );

  // Exported permission: 'delete asset with type slide'.
  $permissions['delete asset with type slide'] = array(
    'name' => 'delete asset with type slide',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'asset',
  );

  // Exported permission: 'edit asset with type slide'.
  $permissions['edit asset with type slide'] = array(
    'name' => 'edit asset with type slide',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'asset',
  );

  // Exported permission: 'update homepage_carousel entityqueue'.
  $permissions['update homepage_carousel entityqueue'] = array(
    'name' => 'update homepage_carousel entityqueue',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'entityqueue',
  );

  return $permissions;
}
