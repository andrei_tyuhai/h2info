<?php
/**
 * @file
 * uswp_slideshow.entityqueue_default.inc
 */

/**
 * Implements hook_entityqueue_default_queues().
 */
function uswp_slideshow_entityqueue_default_queues() {
  $export = array();

  $queue = new EntityQueue();
  $queue->disabled = FALSE; /* Edit this to true to make a default queue disabled initially */
  $queue->api_version = 1;
  $queue->name = 'homepage_carousel';
  $queue->label = 'Homepage carousel';
  $queue->language = 'en';
  $queue->handler = 'simple';
  $queue->target_type = 'asset';
  $queue->settings = array(
    'target_bundles' => array(
      'slide' => 'slide',
    ),
    'min_size' => '0',
    'max_size' => '0',
    'act_as_queue' => 0,
  );
  $export['homepage_carousel'] = $queue;

  return $export;
}
