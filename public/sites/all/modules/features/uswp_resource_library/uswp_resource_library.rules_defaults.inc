<?php
/**
 * @file
 * uswp_resource_library.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uswp_resource_library_default_rules_configuration() {
  $items = array();
  $items['rules_report_broken_resource'] = entity_import('rules_config', '{ "rules_report_broken_resource" : {
      "LABEL" : "Report Broken Resource",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "resource library" ],
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_report_broken_resource" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "flagged-node" ],
            "type" : { "value" : { "resource_profile" : "resource_profile" } }
          }
        }
      ],
      "DO" : [
        { "variable_add" : {
            "USING" : { "type" : "text", "value" : "webmaster@uswaterpartnership.org" },
            "PROVIDE" : { "variable_added" : { "notification_email" : "Notification email" } }
          }
        },
        { "mail" : {
            "to" : "[notification-email:value]",
            "subject" : "A Broken Resource Profile Has Been Reported",
            "message" : "A resource profile has been reported as broken.\\r\\n\\r\\nResource Profile\\r\\nTitle: [flagged-node:title]\\r\\nProfile page: [flagged-node:url]\\r\\nResource url: [flagged-node:field-resource-url]\\r\\n\\r\\nReporting User\\r\\nUsername: [site:current-user]\\r\\nEmail: [flagging-user:mail]\\r\\nAccount roles: [flagging-user:role-names]\\r\\nCountry of Residence: [flagging-user:field-country-of-residence]\\r\\nOrganization: [flagging-user:field-user-organization]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
