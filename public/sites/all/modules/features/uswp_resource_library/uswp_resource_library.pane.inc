<?php
/**
 * @file
 * uswp_resource_library.pane.inc
 */

/**
 * Implements hook_default_pane_container().
 */
function uswp_resource_library_default_pane_container() {
  $export = array();

  $template = new stdClass();
  $template->disabled = FALSE; /* Edit this to true to make a default template disabled initially */
  $template->api_version = 2;
  $template->name = 'post_search_guidance_themes';
  $template->title = 'Post-Search Guidance - Themes';
  $template->plugin = 'text';
  $template->description = 'Further options if searches didn\'t include desired results. Displayed exclusively on Theme/Sub-theme pages.';
  $template->configuration = '';
  $export['post_search_guidance_themes'] = $template;

  return $export;
}
