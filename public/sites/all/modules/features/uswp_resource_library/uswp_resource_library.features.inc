<?php
/**
 * @file
 * uswp_resource_library.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uswp_resource_library_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "pane" && $api == "pane") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uswp_resource_library_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function uswp_resource_library_flag_default_flags() {
  $flags = array();
  // Exported flag: "Bookmarks".
  $flags['bookmarks'] = array(
    'entity_type' => 'node',
    'title' => 'Bookmarks',
    'global' => 0,
    'types' => array(
      0 => 'resource_profile',
    ),
    'flag_short' => 'Bookmark this',
    'flag_long' => 'Add this resource to your personal library on H2infO.',
    'flag_message' => 'This resource has been added to your personal library.',
    'unflag_short' => 'Unbookmark this',
    'unflag_long' => 'Remove this resource from your personal library on H2infO.',
    'unflag_message' => 'This resource has been removed from your personal library.',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'token' => 0,
      'featured' => 0,
      'micro' => 0,
      'meta' => 0,
      'revision' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'uswp_resource_library',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Report Broken Resource".
  $flags['report_broken_resource'] = array(
    'entity_type' => 'node',
    'title' => 'Report Broken Resource',
    'global' => 1,
    'types' => array(
      0 => 'resource_profile',
    ),
    'flag_short' => 'Link Broken?',
    'flag_long' => 'Click here if this resource is not working, is inappropriately categorized, or to report other errors.',
    'flag_message' => 'An administrator has been notified that this resource is broken.',
    'unflag_short' => 'Remove broken indicator',
    'unflag_long' => 'Remove broken indicator',
    'unflag_message' => 'This resource is no longer indicated as broken.',
    'unflag_denied_text' => 'Reported broken',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'token' => 0,
      'featured' => 0,
      'micro' => 0,
      'meta' => 0,
      'revision' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 1,
    'access_author' => '',
    'show_contextual_link' => 1,
    'i18n' => 0,
    'module' => 'uswp_resource_library',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}
