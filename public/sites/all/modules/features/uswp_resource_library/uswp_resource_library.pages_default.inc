<?php
/**
 * @file
 * uswp_resource_library.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function uswp_resource_library_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_2';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'Resource profile',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_bundle:node',
          'settings' => array(
            'type' => array(
              'resource_profile' => 'resource_profile',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'sidebar';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'preface' => NULL,
      'main' => NULL,
      'sidebar' => NULL,
      'featured' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'aff1a20c-197c-432d-b590-3a1c84a1dd1a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2421a05c-ded6-4db6-a298-9eccad0fc409';
    $pane->panel = 'featured';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane--resource-full',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2421a05c-ded6-4db6-a298-9eccad0fc409';
    $display->content['new-2421a05c-ded6-4db6-a298-9eccad0fc409'] = $pane;
    $display->panels['featured'][0] = 'new-2421a05c-ded6-4db6-a298-9eccad0fc409';
    $pane = new stdClass();
    $pane->pid = 'new-f7ae6ae3-083e-4810-8e7c-68b6d146ca06';
    $pane->panel = 'main';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'meta',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f7ae6ae3-083e-4810-8e7c-68b6d146ca06';
    $display->content['new-f7ae6ae3-083e-4810-8e7c-68b6d146ca06'] = $pane;
    $display->panels['main'][0] = 'new-f7ae6ae3-083e-4810-8e7c-68b6d146ca06';
    $pane = new stdClass();
    $pane->pid = 'new-da380d57-f4e7-476e-bf63-59ac93488b08';
    $pane->panel = 'sidebar';
    $pane->type = 'views_panes';
    $pane->subtype = 'related_content-related_resources';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'items_per_page' => '3',
      'context' => array(
        0 => 'argument_entity_id:node_1',
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'da380d57-f4e7-476e-bf63-59ac93488b08';
    $display->content['new-da380d57-f4e7-476e-bf63-59ac93488b08'] = $pane;
    $display->panels['sidebar'][0] = 'new-da380d57-f4e7-476e-bf63-59ac93488b08';
    $pane = new stdClass();
    $pane->pid = 'new-4fa88d17-1a8d-45fc-b51e-4c31e4e482f2';
    $pane->panel = 'sidebar';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_help_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'link_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4fa88d17-1a8d-45fc-b51e-4c31e4e482f2';
    $display->content['new-4fa88d17-1a8d-45fc-b51e-4c31e4e482f2'] = $pane;
    $display->panels['sidebar'][1] = 'new-4fa88d17-1a8d-45fc-b51e-4c31e4e482f2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-2421a05c-ded6-4db6-a298-9eccad0fc409';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context_2'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'term_view_panel_context';
  $handler->task = 'term_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Theme',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'term_vocabulary',
          'settings' => array(
            'machine_name' => array(
              'uswp_themes' => 'uswp_themes',
            ),
          ),
          'context' => 'argument_term_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'sidebar';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'preface' => NULL,
      'main' => NULL,
      'sidebar' => NULL,
      'featured' => array(
        'clean_markup' => array(
          'region_wrapper' => 'div',
          'additional_region_classes' => '',
          'additional_region_attributes' => '',
          'enable_inner_div' => 0,
          'pane_separators' => 0,
        ),
      ),
    ),
    'featured' => array(
      'style' => 'clean_element',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '5e504ad7-d9bb-487b-8219-89574aa28476';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b67e2003-c3f8-45b2-84c5-eca5acdd7b98';
    $pane->panel = 'featured';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => 'page-title',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b67e2003-c3f8-45b2-84c5-eca5acdd7b98';
    $display->content['new-b67e2003-c3f8-45b2-84c5-eca5acdd7b98'] = $pane;
    $display->panels['featured'][0] = 'new-b67e2003-c3f8-45b2-84c5-eca5acdd7b98';
    $pane = new stdClass();
    $pane->pid = 'new-19dedb24-c152-4cb8-95ca-00dd87c0a1bd';
    $pane->panel = 'featured';
    $pane->type = 'entity_view';
    $pane->subtype = 'taxonomy_term';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_term_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '19dedb24-c152-4cb8-95ca-00dd87c0a1bd';
    $display->content['new-19dedb24-c152-4cb8-95ca-00dd87c0a1bd'] = $pane;
    $display->panels['featured'][1] = 'new-19dedb24-c152-4cb8-95ca-00dd87c0a1bd';
    $pane = new stdClass();
    $pane->pid = 'new-f01b3a88-a28d-4f0b-a091-9a89d72262dc';
    $pane->panel = 'featured';
    $pane->type = 'views_panes';
    $pane->subtype = 'term_children-subtheme_pane';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'term_parent',
          'settings' => array(
            'vid' => '7',
          ),
          'context' => 'argument_term_1',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_term_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'f01b3a88-a28d-4f0b-a091-9a89d72262dc';
    $display->content['new-f01b3a88-a28d-4f0b-a091-9a89d72262dc'] = $pane;
    $display->panels['featured'][2] = 'new-f01b3a88-a28d-4f0b-a091-9a89d72262dc';
    $pane = new stdClass();
    $pane->pid = 'new-cd00ce39-32b0-4529-a806-8b7e6bef7054';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-Xd1mTF7YIZOPK4W519miI6mOECakr1Zo';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'term_parent',
          'settings' => array(
            'vid' => '7',
          ),
          'context' => 'argument_term_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Browse by resource type',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane--collapse',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cd00ce39-32b0-4529-a806-8b7e6bef7054';
    $display->content['new-cd00ce39-32b0-4529-a806-8b7e6bef7054'] = $pane;
    $display->panels['main'][0] = 'new-cd00ce39-32b0-4529-a806-8b7e6bef7054';
    $pane = new stdClass();
    $pane->pid = 'new-7c9a0e8f-7e5b-419c-8ae6-7cc15a0df820';
    $pane->panel = 'main';
    $pane->type = 'views_panes';
    $pane->subtype = 'resource_search_results-resource_results';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'arguments' => array(
        'search_api_views_taxonomy_term' => '%term:tid',
        'field_theme_parents_all' => '%term:tid',
      ),
      'override_title' => 1,
      'override_title_text' => '%term:name Resources',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane--resource-search-results',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '7c9a0e8f-7e5b-419c-8ae6-7cc15a0df820';
    $display->content['new-7c9a0e8f-7e5b-419c-8ae6-7cc15a0df820'] = $pane;
    $display->panels['main'][1] = 'new-7c9a0e8f-7e5b-419c-8ae6-7cc15a0df820';
    $pane = new stdClass();
    $pane->pid = 'new-03c3ae08-b2ec-4905-99ee-ed0d8316e8df';
    $pane->panel = 'main';
    $pane->type = 'pane';
    $pane->subtype = 'post_search_guidance_themes';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '03c3ae08-b2ec-4905-99ee-ed0d8316e8df';
    $display->content['new-03c3ae08-b2ec-4905-99ee-ed0d8316e8df'] = $pane;
    $display->panels['main'][2] = 'new-03c3ae08-b2ec-4905-99ee-ed0d8316e8df';
    $pane = new stdClass();
    $pane->pid = 'new-53ac8923-26f3-4338-bc89-7a4712d9a51c';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-Ha4WS3U2oaAkPSPOGgLtMNvnVTppOHAg';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Region/Country',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'sidebar-box',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '53ac8923-26f3-4338-bc89-7a4712d9a51c';
    $display->content['new-53ac8923-26f3-4338-bc89-7a4712d9a51c'] = $pane;
    $display->panels['sidebar'][0] = 'new-53ac8923-26f3-4338-bc89-7a4712d9a51c';
    $pane = new stdClass();
    $pane->pid = 'new-b688ece0-7687-43c9-848f-f35805bc1309';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-Xd1mTF7YIZOPK4W519miI6mOECakr1Zo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Resource Type',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'sidebar-box',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'b688ece0-7687-43c9-848f-f35805bc1309';
    $display->content['new-b688ece0-7687-43c9-848f-f35805bc1309'] = $pane;
    $display->panels['sidebar'][1] = 'new-b688ece0-7687-43c9-848f-f35805bc1309';
    $pane = new stdClass();
    $pane->pid = 'new-83f46339-a44f-40ee-bece-2a53aee6ce89';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-EpcZg0lNSYU6bhgXM1RYoRHxvgryov1K';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'File Type',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'sidebar-box',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '83f46339-a44f-40ee-bece-2a53aee6ce89';
    $display->content['new-83f46339-a44f-40ee-bece-2a53aee6ce89'] = $pane;
    $display->panels['sidebar'][2] = 'new-83f46339-a44f-40ee-bece-2a53aee6ce89';
    $pane = new stdClass();
    $pane->pid = 'new-0655684a-85bd-4182-ad7f-d6b63111b5d5';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'facetapi-szZ3efQQC3kcsCupvOhdNx0r2DSFH7r2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Organization',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'sidebar-box',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '0655684a-85bd-4182-ad7f-d6b63111b5d5';
    $display->content['new-0655684a-85bd-4182-ad7f-d6b63111b5d5'] = $pane;
    $display->panels['sidebar'][3] = 'new-0655684a-85bd-4182-ad7f-d6b63111b5d5';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['term_view_panel_context'] = $handler;

  return $export;
}
