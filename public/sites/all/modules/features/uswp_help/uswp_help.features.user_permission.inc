<?php
/**
 * @file
 * uswp_help.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uswp_help_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uswp_help content'.
  $permissions['create uswp_help content'] = array(
    'name' => 'create uswp_help content',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uswp_help content'.
  $permissions['delete any uswp_help content'] = array(
    'name' => 'delete any uswp_help content',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uswp_help content'.
  $permissions['delete own uswp_help content'] = array(
    'name' => 'delete own uswp_help content',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uswp_help content'.
  $permissions['edit any uswp_help content'] = array(
    'name' => 'edit any uswp_help content',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uswp_help content'.
  $permissions['edit own uswp_help content'] = array(
    'name' => 'edit own uswp_help content',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  return $permissions;
}
