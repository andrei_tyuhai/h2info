<?php
/**
 * @file
 * uswp_help.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uswp_help_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uswp_help_node_info() {
  $items = array(
    'uswp_help' => array(
      'name' => t('Instructional content'),
      'base' => 'node_content',
      'description' => t('<em>Instructional content</em> should be created to provide site visitors with guidance in how to use the resource portal and the content found there.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
