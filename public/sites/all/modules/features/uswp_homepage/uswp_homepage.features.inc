<?php
/**
 * @file
 * uswp_homepage.features.inc
 */

/**
 * Implements hook_ccl_features_preset().
 */
function uswp_homepage_ccl_features_preset() {
  return array(
    'Edit background image' => array(
      'type' => 'view',
      'title' => 'Edit background image',
      'link' => 'admin/structure/pane/homepage_slider_image/edit',
      'options' => 'a:9:{s:12:"advanced_css";s:0:"";s:14:"advanced_query";s:0:"";s:15:"advanced_target";s:7:"default";s:12:"node_options";s:4:"node";s:9:"node_type";s:9:"uswp_help";s:7:"node_id";s:0:"";s:12:"view_options";s:1:"2";s:12:"view_display";s:28:"featured_resources|slideshow";s:9:"view_view";s:15:"related_content";}',
    ),
  );
}

/**
 * Implements hook_ctools_plugin_api().
 */
function uswp_homepage_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "pane" && $api == "pane") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
