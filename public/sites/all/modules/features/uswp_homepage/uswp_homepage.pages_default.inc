<?php
/**
 * @file
 * uswp_homepage.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function uswp_homepage_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'homepage';
  $page->task = 'page';
  $page->admin_title = 'Homepage';
  $page->admin_description = 'Site homepage ';
  $page->path = 'home';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_homepage_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'homepage';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'single';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'preface' => NULL,
      'main' => NULL,
      'sidebar' => NULL,
      'first' => NULL,
      'second' => NULL,
      'third' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '1b3fd012-15d1-46c6-9011-038430ac5a28';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f9312ca1-9ef0-4c23-8c9e-858fb9ac20b4';
    $pane->panel = 'main';
    $pane->type = 'views_panes';
    $pane->subtype = 'term_children-explore';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'explore-themes',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f9312ca1-9ef0-4c23-8c9e-858fb9ac20b4';
    $display->content['new-f9312ca1-9ef0-4c23-8c9e-858fb9ac20b4'] = $pane;
    $display->panels['main'][0] = 'new-f9312ca1-9ef0-4c23-8c9e-858fb9ac20b4';
    $pane = new stdClass();
    $pane->pid = 'new-aa187ce2-aea9-4ed9-8ac0-7e74772f3f98';
    $pane->panel = 'preface';
    $pane->type = 'page_messages';
    $pane->subtype = 'page_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aa187ce2-aea9-4ed9-8ac0-7e74772f3f98';
    $display->content['new-aa187ce2-aea9-4ed9-8ac0-7e74772f3f98'] = $pane;
    $display->panels['preface'][0] = 'new-aa187ce2-aea9-4ed9-8ac0-7e74772f3f98';
    $pane = new stdClass();
    $pane->pid = 'new-7a86d19e-b3a1-4e9a-b0d6-291e72b39bda';
    $pane->panel = 'preface';
    $pane->type = 'pane';
    $pane->subtype = 'homepage_tagline';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '7a86d19e-b3a1-4e9a-b0d6-291e72b39bda';
    $display->content['new-7a86d19e-b3a1-4e9a-b0d6-291e72b39bda'] = $pane;
    $display->panels['preface'][1] = 'new-7a86d19e-b3a1-4e9a-b0d6-291e72b39bda';
    $pane = new stdClass();
    $pane->pid = 'new-e823ca78-1b79-4116-861f-a68eb0b4a415';
    $pane->panel = 'preface';
    $pane->type = 'views_panes';
    $pane->subtype = 'slideshow-homepage_carousel';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'e823ca78-1b79-4116-861f-a68eb0b4a415';
    $display->content['new-e823ca78-1b79-4116-861f-a68eb0b4a415'] = $pane;
    $display->panels['preface'][2] = 'new-e823ca78-1b79-4116-861f-a68eb0b4a415';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['homepage'] = $page;

  return $pages;

}
