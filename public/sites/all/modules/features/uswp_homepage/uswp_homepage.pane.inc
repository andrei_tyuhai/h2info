<?php
/**
 * @file
 * uswp_homepage.pane.inc
 */

/**
 * Implements hook_default_pane_container().
 */
function uswp_homepage_default_pane_container() {
  $export = array();

  $template = new stdClass();
  $template->disabled = FALSE; /* Edit this to true to make a default template disabled initially */
  $template->api_version = 2;
  $template->name = 'homepage_tagline';
  $template->title = 'Homepage tagline';
  $template->plugin = 'text';
  $template->description = 'Prominent tagline display for homepage emphasis.';
  $template->configuration = '';
  $export['homepage_tagline'] = $template;

  return $export;
}
