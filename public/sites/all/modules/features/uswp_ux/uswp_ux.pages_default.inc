<?php
/**
 * @file
 * uswp_ux.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function uswp_ux_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'pm_existing_pages_user_pages_anonymous_panel_context';
  $handler->task = 'pm_existing_pages';
  $handler->subtask = 'user_pages_anonymous';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Default',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        1 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 1,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'single';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'preface' => NULL,
      'main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '66d46d2f-a6a5-4955-978d-763f0aac82d1';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1bfb0a30-690a-4352-853b-f18bf89e70c2';
    $pane->panel = 'main';
    $pane->type = 'pm_existing_pages';
    $pane->subtype = 'pm_existing_pages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      0 => 'task_id',
      'task_id' => 'user_pages_anonymous',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1bfb0a30-690a-4352-853b-f18bf89e70c2';
    $display->content['new-1bfb0a30-690a-4352-853b-f18bf89e70c2'] = $pane;
    $display->panels['main'][0] = 'new-1bfb0a30-690a-4352-853b-f18bf89e70c2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-1bfb0a30-690a-4352-853b-f18bf89e70c2';
  $handler->conf['display'] = $display;
  $export['pm_existing_pages_user_pages_anonymous_panel_context'] = $handler;

  return $export;
}
