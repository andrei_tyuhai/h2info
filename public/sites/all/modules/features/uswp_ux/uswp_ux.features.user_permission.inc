<?php
/**
 * @file
 * uswp_ux.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uswp_ux_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer superfish'.
  $permissions['administer superfish'] = array(
    'name' => 'administer superfish',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'superfish',
  );

  return $permissions;
}
