<?php
/**
 * @file
 * uswp_ux.pm_existing_pages.inc
 */

/**
 * Implements hook_pm_existing_pages_info().
 */
function uswp_ux_pm_existing_pages_info() {
  $export = array();

  $pm_existing_page = new stdClass();
  $pm_existing_page->api_version = 1;
  $pm_existing_page->name = 'user_pages_anonymous';
  $pm_existing_page->label = 'User Pages - Anonymous';
  $pm_existing_page->context = '';
  $pm_existing_page->paths = 'user
user/login
user/register
user/password';
  $export['user_pages_anonymous'] = $pm_existing_page;

  return $export;
}
