<?php
/**
 * @file
 * uswp_ux.pane.inc
 */

/**
 * Implements hook_default_pane_container().
 */
function uswp_ux_default_pane_container() {
  $export = array();

  $template = new stdClass();
  $template->disabled = FALSE; /* Edit this to true to make a default template disabled initially */
  $template->api_version = 2;
  $template->name = 'footer_copyright';
  $template->title = 'Footer copyright';
  $template->plugin = 'text';
  $template->description = 'site copyright info in footer';
  $template->configuration = '';
  $export['footer_copyright'] = $template;

  $template = new stdClass();
  $template->disabled = FALSE; /* Edit this to true to make a default template disabled initially */
  $template->api_version = 2;
  $template->name = 'footer_logo';
  $template->title = 'Footer logo';
  $template->plugin = 'text';
  $template->description = 'U.S. Water Partnership logo';
  $template->configuration = '';
  $export['footer_logo'] = $template;

  return $export;
}

/**
 * Implements hook_default_pane_data().
 */
function uswp_ux_default_pane_data() {
  $export = array();

  $template = new stdClass();
  $template->disabled = FALSE; /* Edit this to true to make a default template disabled initially */
  $template->api_version = 2;
  $template->name = 'footer_copyright_und';
  $template->container = 'footer_copyright';
  $template->lang = 'und';
  $template->data = array(
    'value' => '<p>&copy; 2014 U.S. Water Partnership Web Portal</p>',
    'format' => 'full_html',
    'title' => '<none>',
  );
  $export['footer_copyright_und'] = $template;

  $template = new stdClass();
  $template->disabled = FALSE; /* Edit this to true to make a default template disabled initially */
  $template->api_version = 2;
  $template->name = 'footer_logo_und';
  $template->container = 'footer_logo';
  $template->lang = 'und';
  $template->data = array(
    'value' => '<div><a href="http://uswaterpartnership.org/">U.S. Water Partnership</a></div>',
    'format' => 'full_html',
    'title' => '<none>',
  );
  $export['footer_logo_und'] = $template;

  return $export;
}
