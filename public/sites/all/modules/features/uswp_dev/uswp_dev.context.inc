<?php
/**
 * @file
 * uswp_dev.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uswp_dev_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'masquerade';
  $context->description = 'Masquerade';
  $context->tag = 'development';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'masquerade-masquerade' => array(
          'module' => 'masquerade',
          'delta' => 'masquerade',
          'region' => 'postscript',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Masquerade');
  t('development');
  $export['masquerade'] = $context;

  return $export;
}
