<?php
/**
 * @file
 * uswp_dev.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function uswp_dev_user_default_roles() {
  $roles = array();

  // Exported role: developer.
  $roles['developer'] = array(
    'name' => 'developer',
    'weight' => 3,
  );

  return $roles;
}
