<?php
/**
 * @file
 * uswp_contact.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uswp_contact_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "pm_existing_pages" && $api == "pm_existing_pages") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_entity_rule_setting().
 */
function uswp_contact_default_entity_rule_setting() {
  $items = array();
  $items['1'] = entity_import('entity_rule_setting', '{
    "id" : "1",
    "entity_type" : "entityform",
    "bundle" : "contact",
    "op" : "entityform_submission",
    "rules_config" : "rules_entityform_notification_email_admin",
    "weight" : "0",
    "args" : {
      "email_subject" : "Water Portal Contact Form Submission",
      "email_body" : "A user has provided the submission below through the Water Portal contact form.",
      "show_submission" : 1,
      "to_email" : "webmaster@uswaterpartnership.org"
    },
    "false_msg" : null
  }');
  return $items;
}

/**
 * Implements hook_default_entityform_type().
 */
function uswp_contact_default_entityform_type() {
  $items = array();
  $items['contact'] = entity_import('entityform_type', '{
    "type" : "contact",
    "label" : "Contact Us",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "full_html" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "Contact Us - Thank you",
      "submission_text" : {
        "value" : "\\u003Cp\\u003E\\r\\n\\tThank you for reaching out to the \\u003Ca title=\\u0022Insert this token into your form\\u0022\\u003E[site:name]\\u003C\\/a\\u003E through our contact form. We will be reviewing this as quickly as possible and address it accordingly.\\u003C\\/p\\u003E\\r\\n",
        "format" : "full_html"
      },
      "submission_show_submitted" : 1,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "3" : 0, "4" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Cp\\u003E\\r\\n\\tUsing the form below please contact us to share any feedback about the site or what we do. We value all input and will try to address all feedback accordingly.\\u003C\\/p\\u003E\\r\\n",
        "format" : "full_html"
      },
      "anonymous_links" : 0,
      "session_save" : 1
    },
    "weight" : "0",
    "paths" : {
      "submit" : {
        "source" : "eform\\/submit\\/contact",
        "alias" : "contact",
        "language" : "und"
      },
      "confirm" : {
        "source" : "eform\\/contact\\/confirm",
        "alias" : "contact\\/confirm",
        "language" : "und"
      }
    }
  }');
  return $items;
}
