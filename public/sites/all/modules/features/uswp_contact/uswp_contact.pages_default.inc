<?php
/**
 * @file
 * uswp_contact.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function uswp_contact_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'pm_existing_pages_entityform_panel_context';
  $handler->task = 'pm_existing_pages';
  $handler->subtask = 'entityform';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Default',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'Entityform Type',
        'keyword' => 'entityform_type',
        'name' => 'entity:entityform_type',
        'entity_id' => '1',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'sidebar';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'preface' => NULL,
      'featured' => NULL,
      'main' => NULL,
      'sidebar' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%entityform_type:label';
  $display->uuid = '4ec7a98c-446f-4112-8fa9-1a581a00f98b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-489ef6ec-f765-424e-965b-82aaa08319fa';
    $pane->panel = 'main';
    $pane->type = 'pm_existing_pages';
    $pane->subtype = 'pm_existing_pages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      0 => 'task_id',
      'task_id' => 'entityform',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '489ef6ec-f765-424e-965b-82aaa08319fa';
    $display->content['new-489ef6ec-f765-424e-965b-82aaa08319fa'] = $pane;
    $display->panels['main'][0] = 'new-489ef6ec-f765-424e-965b-82aaa08319fa';
    $pane = new stdClass();
    $pane->pid = 'new-91be9a28-cd27-4434-b670-18a3461c2534';
    $pane->panel = 'preface';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '91be9a28-cd27-4434-b670-18a3461c2534';
    $display->content['new-91be9a28-cd27-4434-b670-18a3461c2534'] = $pane;
    $display->panels['preface'][0] = 'new-91be9a28-cd27-4434-b670-18a3461c2534';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['pm_existing_pages_entityform_panel_context'] = $handler;

  return $export;
}
