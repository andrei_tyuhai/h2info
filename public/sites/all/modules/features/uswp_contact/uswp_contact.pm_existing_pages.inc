<?php
/**
 * @file
 * uswp_contact.pm_existing_pages.inc
 */

/**
 * Implements hook_pm_existing_pages_info().
 */
function uswp_contact_pm_existing_pages_info() {
  $export = array();

  $pm_existing_page = new stdClass();
  $pm_existing_page->api_version = 1;
  $pm_existing_page->name = 'entityform';
  $pm_existing_page->label = 'Entityform';
  $pm_existing_page->context = 'entity|entityform|entityform_id';
  $pm_existing_page->paths = 'eform/submit/%entityform_empty
eform/%entityform_type/confirm
eform/%entityform_type/draft
eform/%entityform_type/submissions';
  $export['entityform'] = $pm_existing_page;

  return $export;
}
