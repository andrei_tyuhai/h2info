<?php
/**
 * @file
 * uswp_seo.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uswp_seo_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer page titles'.
  $permissions['administer page titles'] = array(
    'name' => 'administer page titles',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'page_title',
  );

  // Exported permission: 'administer redirects'.
  $permissions['administer redirects'] = array(
    'name' => 'administer redirects',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'redirect',
  );

  // Exported permission: 'set page title'.
  $permissions['set page title'] = array(
    'name' => 'set page title',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'page_title',
  );

  return $permissions;
}
