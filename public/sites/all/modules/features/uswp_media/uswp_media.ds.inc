<?php
/**
 * @file
 * uswp_media.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function uswp_media_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'asset|slide|full';
  $ds_fieldsetting->entity_type = 'asset';
  $ds_fieldsetting->bundle = 'slide';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'field_slide_image' => array(
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
  );
  $export['asset|slide|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function uswp_media_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'asset|slide|default';
  $ds_layout->entity_type = 'asset';
  $ds_layout->bundle = 'slide';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_slide_title',
        1 => 'field_slide_image',
        2 => 'field_summary',
        3 => 'field_featured_content',
      ),
    ),
    'fields' => array(
      'field_slide_title' => 'ds_content',
      'field_slide_image' => 'ds_content',
      'field_summary' => 'ds_content',
      'field_featured_content' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
  );
  $export['asset|slide|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'asset|slide|full';
  $ds_layout->entity_type = 'asset';
  $ds_layout->bundle = 'slide';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_slide_image',
        1 => 'group_slide_content',
        2 => 'field_slide_title',
        3 => 'field_summary',
        4 => 'field_featured_content',
      ),
    ),
    'fields' => array(
      'field_slide_image' => 'ds_content',
      'group_slide_content' => 'ds_content',
      'field_slide_title' => 'ds_content',
      'field_summary' => 'ds_content',
      'field_featured_content' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
  );
  $export['asset|slide|full'] = $ds_layout;

  return $export;
}
