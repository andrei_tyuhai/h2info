<?php
/**
 * @file
 * uswp_media.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uswp_media_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_slide_content|asset|slide|full';
  $field_group->group_name = 'group_slide_content';
  $field_group->entity_type = 'asset';
  $field_group->bundle = 'slide';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Slide content',
    'weight' => '1',
    'children' => array(
      0 => 'field_featured_content',
      1 => 'field_slide_title',
      2 => 'field_summary',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Slide content',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'asset-slide__content',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_slide_content|asset|slide|full'] = $field_group;

  return $export;
}
