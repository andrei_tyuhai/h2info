<?php
/**
 * @file
 * uswp_user.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uswp_user_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'user-user-field_country_of_residence'
  $field_instances['user-user-field_country_of_residence'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the primary country you reside in.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'taxonomy',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'taxonomy_term_reference_plain',
        'weight' => 4,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'user',
    'fences_wrapper' => 'div',
    'field_name' => 'field_country_of_residence',
    'label' => 'Country of Residence',
    'required' => 1,
    'settings' => array(
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 51,
    ),
  );

  // Exported field_instance: 'user-user-field_organizational_sector'
  $field_instances['user-user-field_organizational_sector'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the primary sector your organization works within.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'taxonomy',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'taxonomy_term_reference_plain',
        'weight' => 3,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'user',
    'fences_wrapper' => 'div',
    'field_name' => 'field_organizational_sector',
    'label' => 'Organizational sector',
    'required' => 1,
    'settings' => array(
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 53,
    ),
  );

  // Exported field_instance: 'user-user-field_regions_of_interest'
  $field_instances['user-user-field_regions_of_interest'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select any regions and/or countries you may be interested in learning more about or following more closely.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'hs_taxonomy',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'hs_taxonomy_term_reference_hierarchical_links',
        'weight' => 4,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 4,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'user',
    'fences_wrapper' => 'div',
    'field_name' => 'field_regions_of_interest',
    'label' => 'Regions of Interest',
    'required' => 0,
    'settings' => array(
      'user_register_form' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'hs_taxonomy',
      'settings' => array(),
      'type' => 'taxonomy_hs',
      'weight' => 54,
    ),
  );

  // Exported field_instance: 'user-user-field_themes_of_interest'
  $field_instances['user-user-field_themes_of_interest'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select any themes and/or sub-themes you may be interested in learning more about or following more closely.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'hs_taxonomy',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'hs_taxonomy_term_reference_hierarchical_links',
        'weight' => 3,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 3,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'user',
    'fences_wrapper' => 'div',
    'field_name' => 'field_themes_of_interest',
    'label' => 'Themes of Interest',
    'required' => 0,
    'settings' => array(
      'user_register_form' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'hs_taxonomy',
      'settings' => array(),
      'type' => 'taxonomy_hs',
      'weight' => 53,
    ),
  );

  // Exported field_instance: 'user-user-field_user_organization'
  $field_instances['user-user-field_user_organization'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter the name of the organization you are most closely affiliated with.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_plain',
        'weight' => 2,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_default',
        'weight' => 2,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'user',
    'fences_wrapper' => 'div',
    'field_name' => 'field_user_organization',
    'label' => 'Organization',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 52,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Country of Residence');
  t('Enter the name of the organization you are most closely affiliated with.');
  t('Organization');
  t('Organizational sector');
  t('Regions of Interest');
  t('Select any regions and/or countries you may be interested in learning more about or following more closely.');
  t('Select any themes and/or sub-themes you may be interested in learning more about or following more closely.');
  t('Select the primary country you reside in.');
  t('Select the primary sector your organization works within.');
  t('Themes of Interest');

  return $field_instances;
}
