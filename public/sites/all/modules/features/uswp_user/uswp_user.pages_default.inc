<?php
/**
 * @file
 * uswp_user.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function uswp_user_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'user_view_panel_context';
  $handler->task = 'user_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Dashboard',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'sidebar';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'preface' => NULL,
      'featured' => NULL,
      'main' => NULL,
      'sidebar' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Your Dashboard';
  $display->uuid = '50179db5-0584-4953-83f0-c547592bdd73';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-69d23624-f1e2-4b82-8de0-8f128d68690e';
    $pane->panel = 'featured';
    $pane->type = 'entity_view';
    $pane->subtype = 'user';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '69d23624-f1e2-4b82-8de0-8f128d68690e';
    $display->content['new-69d23624-f1e2-4b82-8de0-8f128d68690e'] = $pane;
    $display->panels['featured'][0] = 'new-69d23624-f1e2-4b82-8de0-8f128d68690e';
    $pane = new stdClass();
    $pane->pid = 'new-897192a7-5a2f-4d00-96dc-64a925920b0e';
    $pane->panel = 'preface';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '897192a7-5a2f-4d00-96dc-64a925920b0e';
    $display->content['new-897192a7-5a2f-4d00-96dc-64a925920b0e'] = $pane;
    $display->panels['preface'][0] = 'new-897192a7-5a2f-4d00-96dc-64a925920b0e';
    $pane = new stdClass();
    $pane->pid = 'new-1ac5b0d5-1f26-4464-9ae4-ef1a54483507';
    $pane->panel = 'sidebar';
    $pane->type = 'views_panes';
    $pane->subtype = 'bookmarks-short_listing';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:user_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1ac5b0d5-1f26-4464-9ae4-ef1a54483507';
    $display->content['new-1ac5b0d5-1f26-4464-9ae4-ef1a54483507'] = $pane;
    $display->panels['sidebar'][0] = 'new-1ac5b0d5-1f26-4464-9ae4-ef1a54483507';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-69d23624-f1e2-4b82-8de0-8f128d68690e';
  $handler->conf['display'] = $display;
  $export['user_view_panel_context'] = $handler;

  return $export;
}
