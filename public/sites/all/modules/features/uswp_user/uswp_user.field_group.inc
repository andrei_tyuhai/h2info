<?php
/**
 * @file
 * uswp_user.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uswp_user_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content_interests|user|user|default';
  $field_group->group_name = 'group_content_interests';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content Interests',
    'weight' => '2',
    'children' => array(
      0 => 'field_regions_of_interest',
      1 => 'field_themes_of_interest',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Content Interests',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'user-profile__group',
        'description' => 'These settings influence content that may be highlighted or recommended for you.',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_content_interests|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content_interests|user|user|form';
  $field_group->group_name = 'group_content_interests';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content Interests',
    'weight' => '2',
    'children' => array(
      0 => 'field_themes_of_interest',
      1 => 'field_regions_of_interest',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Content Interests',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-content-interests field-group-fieldset',
        'description' => 'These settings influence content that may be highlighted or recommended for you.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_content_interests|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_data|user|user|default';
  $field_group->group_name = 'group_profile_data';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile Data',
    'weight' => '1',
    'children' => array(
      0 => 'field_organizational_sector',
      1 => 'field_country_of_residence',
      2 => 'field_user_organization',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Profile Data',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'user-profile__group',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_profile_data|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_data|user|user|form';
  $field_group->group_name = 'group_profile_data';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile Data',
    'weight' => '1',
    'children' => array(
      0 => 'field_organizational_sector',
      1 => 'field_country_of_residence',
      2 => 'field_user_organization',
      3 => 'timezone',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Profile Data',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-profile-data field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_profile_data|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_system_preferences|user|user|form';
  $field_group->group_name = 'group_system_preferences';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'System Preferences',
    'weight' => '9',
    'children' => array(
      0 => 'masquerade',
      1 => 'ckeditor',
      2 => 'googleanalytics',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'System Preferences',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-system-preferences field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_system_preferences|user|user|form'] = $field_group;

  return $export;
}
