<?php
/**
 * @file
 * uswp_user.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uswp_user_taxonomy_default_vocabularies() {
  return array(
    'organizational_sector' => array(
      'name' => 'Organizational Sector',
      'machine_name' => 'organizational_sector',
      'description' => 'Options for sectors an organization may belong to or work within.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
