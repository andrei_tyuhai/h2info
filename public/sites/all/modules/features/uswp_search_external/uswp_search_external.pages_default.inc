<?php
/**
 * @file
 * uswp_search_external.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function uswp_search_external_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'pm_existing_pages_portal_search_panel_context';
  $handler->task = 'pm_existing_pages';
  $handler->subtask = 'portal_search';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Search USWP Member Sites',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'single';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'preface' => NULL,
      'featured' => NULL,
      'main' => NULL,
      'sidebar' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Search USWP Member Sites';
  $display->uuid = 'f858db21-b478-47a0-a06a-cdb354f9443b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a603d974-9e44-4ec7-a527-edb2ffd28836';
    $pane->panel = 'main';
    $pane->type = 'pm_existing_pages';
    $pane->subtype = 'pm_existing_pages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      0 => 'task_id',
      'task_id' => 'portal_search',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a603d974-9e44-4ec7-a527-edb2ffd28836';
    $display->content['new-a603d974-9e44-4ec7-a527-edb2ffd28836'] = $pane;
    $display->panels['main'][0] = 'new-a603d974-9e44-4ec7-a527-edb2ffd28836';
    $pane = new stdClass();
    $pane->pid = 'new-3f8f45cb-dc7c-4bc9-a027-574caac498bd';
    $pane->panel = 'preface';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3f8f45cb-dc7c-4bc9-a027-574caac498bd';
    $display->content['new-3f8f45cb-dc7c-4bc9-a027-574caac498bd'] = $pane;
    $display->panels['preface'][0] = 'new-3f8f45cb-dc7c-4bc9-a027-574caac498bd';
    $pane = new stdClass();
    $pane->pid = 'new-3d8ce2bc-d42a-4bab-beb2-4efeb0d733bb';
    $pane->panel = 'preface';
    $pane->type = 'page_messages';
    $pane->subtype = 'page_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '3d8ce2bc-d42a-4bab-beb2-4efeb0d733bb';
    $display->content['new-3d8ce2bc-d42a-4bab-beb2-4efeb0d733bb'] = $pane;
    $display->panels['preface'][1] = 'new-3d8ce2bc-d42a-4bab-beb2-4efeb0d733bb';
    $pane = new stdClass();
    $pane->pid = 'new-21d0e211-d0f0-4a35-9479-fc5993c5a706';
    $pane->panel = 'preface';
    $pane->type = 'page_tabs';
    $pane->subtype = 'page_tabs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'type' => 'both',
      'id' => 'tabs',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '21d0e211-d0f0-4a35-9479-fc5993c5a706';
    $display->content['new-21d0e211-d0f0-4a35-9479-fc5993c5a706'] = $pane;
    $display->panels['preface'][2] = 'new-21d0e211-d0f0-4a35-9479-fc5993c5a706';
    $pane = new stdClass();
    $pane->pid = 'new-8b0c571e-80e1-4a37-8be3-0f7fbb1adbc2';
    $pane->panel = 'preface';
    $pane->type = 'pane';
    $pane->subtype = 'portal_search_guidance';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '8b0c571e-80e1-4a37-8be3-0f7fbb1adbc2';
    $display->content['new-8b0c571e-80e1-4a37-8be3-0f7fbb1adbc2'] = $pane;
    $display->panels['preface'][3] = 'new-8b0c571e-80e1-4a37-8be3-0f7fbb1adbc2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-a603d974-9e44-4ec7-a527-edb2ffd28836';
  $handler->conf['display'] = $display;
  $export['pm_existing_pages_portal_search_panel_context'] = $handler;

  return $export;
}
