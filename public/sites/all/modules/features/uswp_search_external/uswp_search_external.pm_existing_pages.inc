<?php
/**
 * @file
 * uswp_search_external.pm_existing_pages.inc
 */

/**
 * Implements hook_pm_existing_pages_info().
 */
function uswp_search_external_pm_existing_pages_info() {
  $export = array();

  $pm_existing_page = new stdClass();
  $pm_existing_page->api_version = 1;
  $pm_existing_page->name = 'portal_search';
  $pm_existing_page->label = 'Portal Search';
  $pm_existing_page->context = '';
  $pm_existing_page->paths = 'search/gss
search/gss/%menu_tail';
  $export['portal_search'] = $pm_existing_page;

  return $export;
}
