<?php
/**
 * @file
 * uswp_search_external.pane.inc
 */

/**
 * Implements hook_default_pane_container().
 */
function uswp_search_external_default_pane_container() {
  $export = array();

  $template = new stdClass();
  $template->disabled = FALSE; /* Edit this to true to make a default template disabled initially */
  $template->api_version = 2;
  $template->name = 'portal_search_guidance';
  $template->title = 'Portal Search Guidance';
  $template->plugin = 'text';
  $template->description = 'Introductory content for the external portal search page.';
  $template->configuration = '';
  $export['portal_search_guidance'] = $template;

  return $export;
}
