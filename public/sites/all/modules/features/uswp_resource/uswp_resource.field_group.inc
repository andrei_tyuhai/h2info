<?php
/**
 * @file
 * uswp_resource.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uswp_resource_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content_actions|node|resource_profile|default';
  $field_group->group_name = 'group_content_actions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_resource_main_content';
  $field_group->data = array(
    'label' => 'Content actions',
    'weight' => '4',
    'children' => array(
      0 => 'field_resource_url',
      1 => 'flag_report_broken_resource',
      2 => 'flag_bookmarks',
      3 => 'service_links_displays_group',
      4 => 'rate_integration_1',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Content actions',
      'instance_settings' => array(
        'id' => 'node_resource_profile_full_group_content_actions',
        'classes' => 'node-type-resource-profile__actions field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_content_actions|node|resource_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content_actions|node|resource_profile|full';
  $field_group->group_name = 'group_content_actions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content actions',
    'weight' => '1',
    'children' => array(
      0 => 'field_resource_url',
      1 => 'service_links_displays_group',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Content actions',
      'instance_settings' => array(
        'classes' => 'group-content-actions field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
        'id' => 'node_resource_profile_full_group_content_actions',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_content_actions|node|resource_profile|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content_actions|node|resource_profile|search_result';
  $field_group->group_name = 'group_content_actions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'search_result';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content actions',
    'weight' => '2',
    'children' => array(
      0 => 'field_resource_url',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Content actions',
      'instance_settings' => array(
        'classes' => 'group-content-actions field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
        'id' => 'node_resource_profile_search_result_group_content_actions',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_content_actions|node|resource_profile|search_result'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content_actions|node|resource_profile|teaser';
  $field_group->group_name = 'group_content_actions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content actions',
    'weight' => '2',
    'children' => array(
      0 => 'field_resource_url',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Content actions',
      'instance_settings' => array(
        'classes' => 'group-content-actions field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
        'id' => 'node_resource_profile_teaser_group_content_actions',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_content_actions|node|resource_profile|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_file_metadata|node|resource_profile|default';
  $field_group->group_name = 'group_file_metadata';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_resource_main_content';
  $field_group->data = array(
    'label' => 'File metadata',
    'weight' => '5',
    'children' => array(
      0 => 'field_file_size',
      1 => 'field_file_type',
      2 => 'field_organization',
      3 => 'field_pub_date',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'File metadata',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-file-metadata field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_file_metadata|node|resource_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_metadata|node|resource_profile|micro';
  $field_group->group_name = 'group_metadata';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'micro';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Metadata',
    'weight' => '1',
    'children' => array(
      0 => 'field_organization',
      1 => 'field_pub_date',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Metadata',
      'instance_settings' => array(
        'classes' => 'group-metadata field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
        'id' => 'node_resource_profile_micro_group_metadata',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_metadata|node|resource_profile|micro'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_metadata|node|resource_profile|search_result';
  $field_group->group_name = 'group_metadata';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'search_result';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Metadata',
    'weight' => '1',
    'children' => array(
      0 => 'field_organization',
      1 => 'field_pub_date',
      2 => 'flag_bookmarks',
      3 => 'rate_integration_1',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Metadata',
      'instance_settings' => array(
        'id' => 'node_resource_profile_search_result_group_metadata',
        'classes' => 'group-metadata field-group-fieldset',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_metadata|node|resource_profile|search_result'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_metadata|node|resource_profile|teaser';
  $field_group->group_name = 'group_metadata';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Metadata',
    'weight' => '1',
    'children' => array(
      0 => 'field_organization',
      1 => 'field_pub_date',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Metadata',
      'instance_settings' => array(
        'classes' => 'group-metadata field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
        'id' => 'node_resource_profile_teaser_group_metadata',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_metadata|node|resource_profile|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_meta|node|resource_profile|meta';
  $field_group->group_name = 'group_meta';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'meta';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'group-meta',
    'weight' => '0',
    'children' => array(
      0 => 'field_data_scale',
      1 => 'field_regional_focus',
      2 => 'field_related_resources',
      3 => 'field_resource_type',
      4 => 'field_theme',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'group-meta',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'node-type-resource-profile__meta',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_meta|node|resource_profile|meta'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_resource_categorization|node|resource_profile|form';
  $field_group->group_name = 'group_resource_categorization';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_resource_data';
  $field_group->data = array(
    'label' => 'Categorization',
    'weight' => '3',
    'children' => array(
      0 => 'field_data_scale',
      1 => 'field_nexus_tags',
      2 => 'field_organization',
      3 => 'field_regional_focus',
      4 => 'field_resource_type',
      5 => 'field_theme',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_resource_categorization|node|resource_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_resource_data|node|resource_profile|form';
  $field_group->group_name = 'group_resource_data';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Resource Data',
    'weight' => '1',
    'children' => array(
      0 => 'group_resource_details',
      1 => 'group_resource_categorization',
      2 => 'group_resource_related',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-resource-data field-group-htabs',
      ),
    ),
  );
  $export['group_resource_data|node|resource_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_resource_details|node|resource_profile|form';
  $field_group->group_name = 'group_resource_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_resource_data';
  $field_group->data = array(
    'label' => 'Resource Details',
    'weight' => '2',
    'children' => array(
      0 => 'field_pub_date',
      1 => 'field_resource_summary',
      2 => 'field_resource_url',
      3 => 'group_resource_file_details',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Resource Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_resource_details|node|resource_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_resource_file_details|node|resource_profile|form';
  $field_group->group_name = 'group_resource_file_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_resource_details';
  $field_group->data = array(
    'label' => 'File Details',
    'weight' => '9',
    'children' => array(
      0 => 'field_file_size',
      1 => 'field_file_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'File Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-resource-file-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_resource_file_details|node|resource_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_resource_main_content|node|resource_profile|default';
  $field_group->group_name = 'group_resource_main_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Main Content',
    'weight' => '0',
    'children' => array(
      0 => 'field_resource_summary',
      1 => 'title',
      2 => 'theme_indicators',
      3 => 'group_content_actions',
      4 => 'group_file_metadata',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Main Content',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-resource-main-content',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_resource_main_content|node|resource_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_resource_related|node|resource_profile|form';
  $field_group->group_name = 'group_resource_related';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resource_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_resource_data';
  $field_group->data = array(
    'label' => 'Related Content',
    'weight' => '4',
    'children' => array(
      0 => 'field_related_resources',
      1 => 'field_help_content',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-resource-related field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_resource_related|node|resource_profile|form'] = $field_group;

  return $export;
}
