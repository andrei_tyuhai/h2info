<?php
/**
 * @file
 * uswp_resource.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uswp_resource_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create resource_profile content'.
  $permissions['create resource_profile content'] = array(
    'name' => 'create resource_profile content',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any resource_profile content'.
  $permissions['delete any resource_profile content'] = array(
    'name' => 'delete any resource_profile content',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own resource_profile content'.
  $permissions['delete own resource_profile content'] = array(
    'name' => 'delete own resource_profile content',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uswp_data_scale'.
  $permissions['delete terms in uswp_data_scale'] = array(
    'name' => 'delete terms in uswp_data_scale',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uswp_file_types'.
  $permissions['delete terms in uswp_file_types'] = array(
    'name' => 'delete terms in uswp_file_types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uswp_nexus_tags'.
  $permissions['delete terms in uswp_nexus_tags'] = array(
    'name' => 'delete terms in uswp_nexus_tags',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uswp_organizations'.
  $permissions['delete terms in uswp_organizations'] = array(
    'name' => 'delete terms in uswp_organizations',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uswp_regions'.
  $permissions['delete terms in uswp_regions'] = array(
    'name' => 'delete terms in uswp_regions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uswp_resource_types'.
  $permissions['delete terms in uswp_resource_types'] = array(
    'name' => 'delete terms in uswp_resource_types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uswp_themes'.
  $permissions['delete terms in uswp_themes'] = array(
    'name' => 'delete terms in uswp_themes',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any resource_profile content'.
  $permissions['edit any resource_profile content'] = array(
    'name' => 'edit any resource_profile content',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own resource_profile content'.
  $permissions['edit own resource_profile content'] = array(
    'name' => 'edit own resource_profile content',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uswp_data_scale'.
  $permissions['edit terms in uswp_data_scale'] = array(
    'name' => 'edit terms in uswp_data_scale',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uswp_file_types'.
  $permissions['edit terms in uswp_file_types'] = array(
    'name' => 'edit terms in uswp_file_types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uswp_nexus_tags'.
  $permissions['edit terms in uswp_nexus_tags'] = array(
    'name' => 'edit terms in uswp_nexus_tags',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uswp_organizations'.
  $permissions['edit terms in uswp_organizations'] = array(
    'name' => 'edit terms in uswp_organizations',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uswp_regions'.
  $permissions['edit terms in uswp_regions'] = array(
    'name' => 'edit terms in uswp_regions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uswp_resource_types'.
  $permissions['edit terms in uswp_resource_types'] = array(
    'name' => 'edit terms in uswp_resource_types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uswp_themes'.
  $permissions['edit terms in uswp_themes'] = array(
    'name' => 'edit terms in uswp_themes',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'flag report_broken_resource'.
  $permissions['flag report_broken_resource'] = array(
    'name' => 'flag report_broken_resource',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'developer' => 'developer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unflag report_broken_resource'.
  $permissions['unflag report_broken_resource'] = array(
    'name' => 'unflag report_broken_resource',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'flag',
  );

  return $permissions;
}
