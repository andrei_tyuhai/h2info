<?php
/**
 * @file
 * uswp_resource.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uswp_resource_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uswp_resource_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uswp_resource_node_info() {
  $items = array(
    'resource_profile' => array(
      'name' => t('Resource profile'),
      'base' => 'node_content',
      'description' => t('A <em>resource profile</em> gathers and presents metadata about a resource to be made available through the resource portal.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
