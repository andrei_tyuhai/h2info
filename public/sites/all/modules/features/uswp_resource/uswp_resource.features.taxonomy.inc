<?php
/**
 * @file
 * uswp_resource.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uswp_resource_taxonomy_default_vocabularies() {
  return array(
    'uswp_data_scale' => array(
      'name' => 'Data Scale',
      'machine_name' => 'uswp_data_scale',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'uswp_file_types' => array(
      'name' => 'File type',
      'machine_name' => 'uswp_file_types',
      'description' => 'Taxonomy for file types',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'uswp_nexus_tags' => array(
      'name' => 'Nexus Tags',
      'machine_name' => 'uswp_nexus_tags',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'uswp_organizations' => array(
      'name' => 'Organization',
      'machine_name' => 'uswp_organizations',
      'description' => 'Taxonomy for organizations',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'uswp_regions' => array(
      'name' => 'Region',
      'machine_name' => 'uswp_regions',
      'description' => 'Taxonomy for regions',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'uswp_resource_types' => array(
      'name' => 'Resource type',
      'machine_name' => 'uswp_resource_types',
      'description' => 'Taxonomy for resource types',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'uswp_themes' => array(
      'name' => 'Theme',
      'machine_name' => 'uswp_themes',
      'description' => 'Taxonomy for themes vocabulary',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
