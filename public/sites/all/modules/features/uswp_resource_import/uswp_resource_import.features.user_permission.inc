<?php
/**
 * @file
 * uswp_resource_import.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uswp_resource_import_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'import resource_profile_batch_upload feeds'.
  $permissions['import resource_profile_batch_upload feeds'] = array(
    'name' => 'import resource_profile_batch_upload feeds',
    'roles' => array(
      'administrator' => 'administrator',
      'developer' => 'developer',
    ),
    'module' => 'feeds',
  );

  return $permissions;
}
