<?php
/**
 * @file
 * uswp_resource_import.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function uswp_resource_import_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'resource_profile_batch_upload';
  $feeds_importer->config = array(
    'name' => 'Resource Profile Batch Upload',
    'description' => 'Batch upload importer for resource profiles.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => 0,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Theme 1',
            'target' => 'field_theme',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          2 => array(
            'source' => 'Theme 2',
            'target' => 'field_theme',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          3 => array(
            'source' => 'Theme 3',
            'target' => 'field_theme',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          4 => array(
            'source' => 'Theme 4',
            'target' => 'field_theme',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          5 => array(
            'source' => 'Theme 5',
            'target' => 'field_theme',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          6 => array(
            'source' => 'Theme 6',
            'target' => 'field_theme',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          7 => array(
            'source' => 'Theme 7',
            'target' => 'field_theme',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          8 => array(
            'source' => 'Theme 8',
            'target' => 'field_theme',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          9 => array(
            'source' => 'Resource Link',
            'target' => 'field_resource_url:url',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'Summary',
            'target' => 'field_resource_summary',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'Organization 1',
            'target' => 'field_organization',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          12 => array(
            'source' => 'Organization 2',
            'target' => 'field_organization',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          13 => array(
            'source' => 'Organization 3',
            'target' => 'field_organization',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          14 => array(
            'source' => 'Organization 4',
            'target' => 'field_organization',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          15 => array(
            'source' => 'Organization 5',
            'target' => 'field_organization',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          16 => array(
            'source' => 'Publication Date',
            'target' => 'field_pub_date:start',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'Resource Type',
            'target' => 'field_resource_type',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          18 => array(
            'source' => 'File Type',
            'target' => 'field_file_type',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          19 => array(
            'source' => 'File Size',
            'target' => 'field_file_size',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'Region/Country 1',
            'target' => 'field_regional_focus',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          21 => array(
            'source' => 'Region/Country 2',
            'target' => 'field_regional_focus',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          22 => array(
            'source' => 'Region/Country 3',
            'target' => 'field_regional_focus',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          23 => array(
            'source' => 'Nexus 1',
            'target' => 'field_nexus_tags',
            'unique' => FALSE,
          ),
          24 => array(
            'source' => 'Nexus 2',
            'target' => 'field_nexus_tags',
            'unique' => FALSE,
          ),
          25 => array(
            'source' => 'Nexus 3',
            'target' => 'field_nexus_tags',
            'unique' => FALSE,
          ),
          26 => array(
            'source' => 'Nexus 4',
            'target' => 'field_nexus_tags',
            'unique' => FALSE,
          ),
          27 => array(
            'source' => 'Nexus 5',
            'target' => 'field_nexus_tags',
            'unique' => FALSE,
          ),
          28 => array(
            'source' => 'Linked Resources 1',
            'target' => 'field_related_resources:label',
            'unique' => FALSE,
          ),
          29 => array(
            'source' => 'Linked Resources 2',
            'target' => 'field_related_resources:label',
            'unique' => FALSE,
          ),
          30 => array(
            'source' => 'Linked Resources 3',
            'target' => 'field_related_resources:label',
            'unique' => FALSE,
          ),
          31 => array(
            'source' => 'Resource Scale',
            'target' => 'field_data_scale',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          32 => array(
            'source' => 'Help Content',
            'target' => 'field_help_content:url',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'resource_profile',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['resource_profile_batch_upload'] = $feeds_importer;

  return $export;
}
