<?php
/**
 * @file
 * uswp_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function uswp_core_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -29;
  $handler->conf = array(
    'title' => 'Default',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'sidebar';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'preface' => NULL,
      'main' => NULL,
      'sidebar' => NULL,
      'featured' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '0ea3cc0b-6a81-4098-bb00-b9efbee5a642';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-13fc7ddd-6372-48e4-a9bd-8cc867bcac45';
    $pane->panel = 'main';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '13fc7ddd-6372-48e4-a9bd-8cc867bcac45';
    $display->content['new-13fc7ddd-6372-48e4-a9bd-8cc867bcac45'] = $pane;
    $display->panels['main'][0] = 'new-13fc7ddd-6372-48e4-a9bd-8cc867bcac45';
    $pane = new stdClass();
    $pane->pid = 'new-f7479165-6c54-47a2-ac75-92d3880a0298';
    $pane->panel = 'sidebar';
    $pane->type = 'menu_tree';
    $pane->subtype = 'main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'menu_name' => 'main-menu',
      'parent_mlid' => 0,
      'parent' => 'main-menu:0',
      'title_link' => 1,
      'admin_title' => '',
      'level' => '2',
      'follow' => 0,
      'depth' => '0',
      'expanded' => 0,
      'sort' => 0,
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane--secondary-menu',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f7479165-6c54-47a2-ac75-92d3880a0298';
    $display->content['new-f7479165-6c54-47a2-ac75-92d3880a0298'] = $pane;
    $display->panels['sidebar'][0] = 'new-f7479165-6c54-47a2-ac75-92d3880a0298';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-13fc7ddd-6372-48e4-a9bd-8cc867bcac45';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  return $export;
}
