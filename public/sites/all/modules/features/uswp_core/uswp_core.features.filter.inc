<?php
/**
 * @file
 * uswp_core.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function uswp_core_filter_default_formats() {
  $formats = array();

  // Exported format: Plain text.
  $formats['plain_text'] = array(
    'format' => 'plain_text',
    'name' => 'Plain text',
    'cache' => 1,
    'status' => 1,
    'weight' => 10,
    'filters' => array(
      'filter_html_escape' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => 1,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_autop' => array(
        'weight' => 2,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => 50,
        'status' => 1,
        'settings' => array(
          'local_paths' => 'http://localhost:8080/
http://uswp-dev.gotpantheon.com/
http://uswp-test.gotpantheon.com/
http://uswp-live.gotpantheon.com/',
          'protocol_style' => 'full',
        ),
      ),
    ),
  );

  return $formats;
}
