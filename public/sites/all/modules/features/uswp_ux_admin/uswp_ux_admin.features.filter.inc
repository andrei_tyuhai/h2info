<?php
/**
 * @file
 * uswp_ux_admin.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function uswp_ux_admin_filter_default_formats() {
  $formats = array();

  // Exported format: Full HTML.
  $formats['full_html'] = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'cache' => 0,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'assets_filter' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => 50,
        'status' => 1,
        'settings' => array(
          'local_paths' => 'http://localhost:8080/
http://uswp-dev.gotpantheon.com/
http://uswp-test.gotpantheon.com/
http://uswp-live.gotpantheon.com/',
          'protocol_style' => 'full',
        ),
      ),
    ),
  );

  // Exported format: Partial HTML.
  $formats['partial_html'] = array(
    'format' => 'partial_html',
    'name' => 'Partial HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'filter_html' => array(
        'weight' => -10,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
      'filter_autop' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => 50,
        'status' => 1,
        'settings' => array(
          'local_paths' => 'http://localhost:8080/
http://uswp-dev.gotpantheon.com/
http://uswp-test.gotpantheon.com/
http://uswp-live.gotpantheon.com/',
          'protocol_style' => 'full',
        ),
      ),
    ),
  );

  return $formats;
}
