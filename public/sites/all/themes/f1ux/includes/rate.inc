<?php
/**
 * @file
 * Rate-related functions.
 */

/**
 * Preprocess function for rate widgets.
 *
 * Add special handling for Like button styling of content_rating widget.
 */
function f1ux_preprocess_rate_widget(&$vars) {
  // Limit customization only to content_rating widget
  if ($vars['theme_hook_suggestion'] == 'rate_widget__content_rating') {
    $results = $vars['results'];

    // Add class based on user's vote
    $vars['classes_array'][] = empty($results['user_vote']) ? 'user-no-vote' : 'user-voted';

    // Flag if no votes have been cast yet
    if ($results['count'] == 0) {
      $vars['no_votes'] = TRUE;
      $vars['classes_array'][] = 'no-votes';
    }

    // Remove .rate-widget class to avoid conflicts in js
    foreach (array_keys($vars['classes_array'], 'rate-widget') as $key) {
      unset($vars['classes_array'][$key]);
    }

    // Set info string appropriately
    if (empty($vars['no_votes'])) {
      $vars['info'] = '';
    }
    else {
      $vars['info'] = t('Be the first to vote.');
    }
  }
}
