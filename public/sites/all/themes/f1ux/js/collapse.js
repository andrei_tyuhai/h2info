(function ($) {

/**
 * Toggle the visibility of a fieldset using smooth animations.
 */
Drupal.toggleFieldset = function (fieldset) {
  var $fieldset = $(fieldset);
  if ($fieldset.is('.is-collapsed')) {
    var $content = $('> .fieldset__content', fieldset).hide();
    $fieldset
      .removeClass('is-collapsed')
      .trigger({ type: 'collapsed', value: false })
      .find('.fieldset__legend-link > .element-invisible').html(Drupal.t('Hide'));
    $content.slideDown({
      duration: 'fast',
      easing: 'linear',
      complete: function () {
        Drupal.collapseScrollIntoView(fieldset);
        fieldset.animating = false;
      },
      step: function () {
        // Scroll the fieldset into view.
        Drupal.collapseScrollIntoView(fieldset);
      }
    });
  }
  else {
    $fieldset.trigger({ type: 'collapsed', value: true });
    $('> .fieldset__content', fieldset).slideUp('fast', function () {
      $fieldset
        .addClass('is-collapsed')
        .find('.fieldset__legend-link > .element-invisible').html(Drupal.t('Show'));
      fieldset.animating = false;
    });
  }
};

/**
 * Scroll a given fieldset into view as much as possible.
 */
Drupal.collapseScrollIntoView = function (node) {
  var h = document.documentElement.clientHeight || document.body.clientHeight || 0;
  var offset = document.documentElement.scrollTop || document.body.scrollTop || 0;
  var posY = $(node).offset().top;
  var fudge = 55;
  if (posY + node.offsetHeight + fudge > h + offset) {
    if (node.offsetHeight > h) {
      window.scrollTo(0, posY);
    }
    else {
      window.scrollTo(0, posY + node.offsetHeight - h + fudge);
    }
  }
};

/**
 * Make Solr search facets collapsible.
 */
function collapseFacets(selector) {
  $(selector).each(function(){
    
  });
}

Drupal.behaviors.collapse = {
  attach: function (context, settings) {

    // Collapsible Solr facets
    $('div[class*="pane--facetapi-"] li.expanded').each(function(){
      var me = $(this);
      me.prepend('<a class="control plus">+</a>');
      var content = me.children('.item-list');
      var control = me.find('.control');
      content.hide(); // collapse all by default
      control.click(function(){
        if (control.is('.plus')) {
          content.slideDown();
          control.text('-').addClass('minus').removeClass('plus');
          $(this).blur();
          return false;
        } else {
          content.slideUp();
          control.text('+').addClass('plus').removeClass('minus');
          $(this).blur();
          return false;
        }
      });
      if (me.find('.facetapi-active').length > 0) { // open filter if active
        control.click();
      }
    });

    // Collapsible panes
    $('.pane--collapse').each(function(){
      var me = $(this);
      var title = me.find('.pane__title');
      title.prepend('<span class="control--inline plus">+</span>');
      title.wrapInner('<a></a>')
      var clicker = title.find('a');
      var control = title.find('.control--inline');
      var content = me.find('.pane__content');
      content.hide();
      clicker.click(function(){
        if (control.is('.plus')) {
          me.addClass('open');
          content.slideDown(400);
          control.text('-').addClass('minus').removeClass('plus');
          $(this).blur();
          return false;
        } else {
          content.slideUp(400, function(){
            me.removeClass('open');
          });
          control.text('+').addClass('plus').removeClass('minus');
          $(this).blur();
          return false;
        }
      })
    });

  }
};

})(jQuery);
