// Custom scripts file
// to load, uncomment the call to this file in f1ux.info

(function ($) {

  // set equal heights on sidebar/main content area
  function sidebarHeight() {
    if ($('.l-sidebar').length > 0) {
      $('.l-sidebar,.l-main-wrapper').removeAttr('style');
      if (Modernizr.mq('(min-width: 700px)')) { // corresponds to where panels layout collapses
        var sidebar = $('.l-sidebar').height();
        var main = $('.l-main-wrapper').height();
        if (sidebar < main) {
          $('.l-sidebar').css("min-height",main+"px");
        }
      }
    }
  }

  // Generic function that runs on window resize.
  function resizeStuff() {
    sidebarHeight();
  }

  // Runs function once on window resize.
  var TO = false;
  $(window).resize(function () {
    if (TO !== false) {
      clearTimeout(TO);
    }

    // 200 is time in miliseconds.
    TO = setTimeout(resizeStuff, 200);
  }).resize();

  Drupal.behaviors.general = {
    attach: function() {

      // jumplink on homepage
      $('.explore-themes h2.pane__title').click(function(){
        if (Modernizr.mq('(min-width: 700px)')) {
          var menuheight = 214; // height of normal header
        } else {
          var menuheight = 198; // height of mobile header
        }
        $('html, body').animate({
          scrollTop: $('.explore-themes').offset().top - menuheight
        }, 500);
        return false;
      });

      // remove focus for flexslider nav when mouse leaves
      $('.flex-direction-nav a').mouseleave(function(){
        $(this).blur();
      });

    }
  }

  Drupal.behaviors.search = {
    attach: function() {

      // placeholder fallback
      if (!Modernizr.input.placeholder) {
        $('[placeholder]').focus(function() {
          var input = $(this);
          if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
          }
        }).blur(function() {
          var input = $(this);
          if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
          }
        }).blur().parents('form').submit(function() {
          $(this).find('[placeholder]').each(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
              input.val('');
            }
          })
        });
      }

      // add placeholder text to main search bar
      $('.site-search input.form-text').attr('placeholder','Search');
      // add placeholder text to Views search bar
      $('.view--resource-search-results input.form-text').attr('placeholder','Search this theme');

    }
  }


})(jQuery);
