<?php
/**
 * Implements hook_panels_layouts().
 */
function f1ux_single_panels_layouts() {
  $items['single'] = array(
    'title' => t('Preface and single col'),
    'category' => t('Responsive'),
    'icon' => 'single.png',
    'theme' => 'single',
    'admin css' => '../../../css/panels-admin/single.css',
    'regions' => array(
      'preface' => t('Preface'),
      'main' => t('Main column'),
    ),
  );

  return $items;
}
