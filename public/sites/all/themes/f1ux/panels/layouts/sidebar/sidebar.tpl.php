<?php
  $preface = (empty($content['preface']) ? 'preface-empty' : 'preface');
  $featured = (empty($content['featured']) ? 'featured-empty' : 'featured');
  $main = (empty($content['main']) ? 'main-empty' : 'main');
  $sidebar = (empty($content['sidebar']) ? 'sidebar-empty' : 'sidebar');
  $classes = "$preface $featured $main $sidebar";
?>

<div<?php if (!empty($css_id)): ?> id="<?php print $css_id; ?>"<?php endif; ?> class="l-panels-sidebar <?php print $classes; ?>">
  <?php if (!empty($content['preface'])): ?>
    <div class="l-preface">
      <div class="l-constrain">
        <?php print $content['preface']; ?>
      </div>
    </div>
  <?php endif; ?>
  <div class="l-constrain">
    <?php if (!empty($content['main']) || (!empty($content['featured']))): ?>
      <div class="l-main-wrapper">
        <?php if (!empty($content['featured'])): ?>
          <div class="l-featured">
            <div class="featured-area">
              <?php print $content['featured']; ?>
            </div>
          </div>
        <?php endif; ?>
        <?php if (!empty($content['main'])): ?>
          <div class="l-main">
            <?php print $content['main']; ?>
          </div>
        <?php endif; ?>
      </div>
    <?php endif; ?>
    <?php if (!empty($content['sidebar'])): ?>
      <aside class="l-sidebar">
        <?php print $content['sidebar']; ?>
      </aside>
    <?php endif; ?>
  </div>
</div>

