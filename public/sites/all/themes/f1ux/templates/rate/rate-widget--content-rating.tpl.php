<?php
/**
 * @file
 * Rate widget theme
 */
?>
<div class="<?php print $classes; ?>">
  <div class="button">
    <?php print $buttons[0]; ?><span class="count">(<?php print $results['count']; ?>)</span>
  </div>
<?php if ($info): ?>
  <span class="info"><?php print $info; ?></span>
<?php endif; ?>
</div>
