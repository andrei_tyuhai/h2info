<?php
/**
 * Enable development modules through environment_modules.
 */
$conf['environment_modules'] = array_intersect_key($contextual_modules, array(
  // Development modules
  'devel' => 'devel',
  'masquerade' => 'masquerade',
  'maillog' => 'maillog',
  'uswp_dev' => 'uswp_dev',

  // UI modules
  'context_ui' => 'context_ui',
  'ds_ui' => 'ds_ui',
  'field_ui' => 'field_ui',
  'rules_admin' => 'rules_admin',
  'views_ui' => 'views_ui',
));

/**
 * Disable unnecessary modules through environment_modules.
 *
 * Note: Ensure this patch is applied - https://www.drupal.org/node/2103649
 */
$conf['environment_modules_disable'] = array_intersect_key($contextual_modules, array(
  'coder' => 'coder',
));

/**
 * Ensure correct Google Analytics property for tracking
 */
$conf['googleanalytics_account'] = 'UA-32817287-2';

// Anonymous caching.
$conf['cache'] = 0;
// Block caching - disabled.
$conf['block_cache'] = 0;
// Expiration of cached pages - none.
$conf['page_cache_maximum_age'] = 0;
// Aggregate and compress CSS files in Drupal - off.
$conf['preprocess_css'] = 0;
// Aggregate JavaScript files in Drupal - off.
$conf['preprocess_js'] = 0;
// Minimum cache lifetime - always none.
$conf['cache_lifetime'] = 0;
// Cached page compression - always off.
$conf['page_compression'] = 0;

/**
 * Include Pantheon platform-wide configuration.
 */
include 'settings.pantheon.php';
