<?php
/**
 * Database configuration
 */
$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'web',
  'username' => 'web',
  'password' => 'web',
  'host' => 'localhost',
  'prefix' => '',
);

/**
 * Solr Search API overrides
 *
 * Configuration setting per Solr instance
 */
$conf['search_api_override_mode'] = 'load';
$conf['search_api_solr_overrides'] = array(
  'solr_server' => array(
    'name' => t('Solr Server (Virtual Machine)'),
    'options' => array(
      'host' => 'localhost',
      'port' => 8983,
      'path' => '/solr/drupal'
    )
  )
);

/**
 * Enable development modules through environment_modules.
 */
$conf['environment_modules'] = array_intersect_key($contextual_modules, array(
  // Development modules
  'devel' => 'devel',
  'coder' => 'coder',
  'maillog' => 'maillog',
  'masquerade' => 'masquerade',
  'search_krumo' => 'search_krumo',
  'uswp_dev' => 'uswp_dev',

  // UI modules
  'context_ui' => 'context_ui',
  'ds_ui' => 'ds_ui',
  'field_ui' => 'field_ui',
  'rules_admin' => 'rules_admin',
  'views_ui' => 'views_ui',
));

/**
 * Disable unnecessary modules through environment_modules.
 *
 * Note: Ensure this patch is applied - https://www.drupal.org/node/2103649
 */
$conf['environment_modules_disable'] = array_intersect_key($contextual_modules, array(
  'pantheon_apachesolr' => 'pantheon_apachesolr',
  'pantheon_api' => 'pantheon_api',
));

/**
 * Prevent Google Analytics tracking of development environments
 */
$conf['googleanalytics_account'] = '';

/**
 * Disable caching and asset preprocessing for development
 */
$conf['cache'] = FALSE;
$conf['page_compression'] = FALSE;
$conf['preprocess_css'] = FALSE;
$conf['css_gzip'] = FALSE;
$conf['preprocess_js'] = FALSE;
$conf['javascript_aggregator_gzip'] = FALSE;
