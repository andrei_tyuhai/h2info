<?php
/**
 * @file
 * Universal settings required for all Pantheon environments.
 */

// Ensure Pantheon modules are enabled
$conf['environment_modules']['pantheon_api'] = $contextual_modules['pantheon_api'];
$conf['environment_modules']['pantheon_apachesolr'] = $contextual_modules['pantheon_apachesolr'];

// Enable Redis module for back-end caching
$conf['environment_modules']['redis'] = $contextual_modules['redis'];

// Configure Redis caching
$conf['redis_client_interface'] = 'PhpRedis';
$conf['cache_backends'][] = 'sites/all/modules/contrib/redis/redis.autoload.inc';
$conf['cache_default_class'] = 'Redis_Cache';
$conf['cache_prefix'] = array('default' => 'pantheon-redis');
// Do not use Redis for cache_form (no performance difference).
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
// Use Redis for Drupal locks (semaphore).
$conf['lock_inc'] = 'sites/all/modules/contrib/redis/redis.lock.inc';
