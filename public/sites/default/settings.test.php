<?php
/**
 * Enable development modules through environment_modules.
 */
$conf['environment_modules'] = array_intersect_key($contextual_modules, array(
  // Development modules
  'masquerade' => 'masquerade',
  'maillog' => 'maillog',
  'uswp_dev' => 'uswp_dev',
));

/**
 * Disable unnecessary modules through environment_modules.
 *
 * Note: Ensure this patch is applied - https://www.drupal.org/node/2103649
 */
$conf['environment_modules_disable'] = array_intersect_key($contextual_modules, array(
  // Development modules
  'devel' => 'devel',
  'coder' => 'coder',
  'search_krumo' => 'search_krumo',
  'uswp_dev' => 'uswp_dev',

  // UI modules
  'context_ui' => 'context_ui',
  'ds_ui' => 'ds_ui',
  'field_ui' => 'field_ui',
  'rules_admin' => 'rules_admin',
  'views_ui' => 'views_ui',
));

/**
 * Ensure correct Google Analytics property for tracking
 */
$conf['googleanalytics_account'] = 'UA-32817287-3';

// Anonymous caching - enabled.
$conf['cache'] = 1;
// Block caching - enabled.
$conf['block_cache'] = 1;
// Expiration of cached pages - 15 minutes.
$conf['page_cache_maximum_age'] = 900;
// Aggregate and compress CSS files in Drupal - on.
$conf['preprocess_css'] = 1;
// Aggregate JavaScript files in Drupal - on.
$conf['preprocess_js'] = 1;
// Minimum cache lifetime - always none.
$conf['cache_lifetime'] = 0;
// Cached page compression - always off.
$conf['page_compression'] = 0;

/**
 * Include Pantheon platform-wide configuration.
 */
include 'settings.pantheon.php';
